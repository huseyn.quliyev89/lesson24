package az.ingress.employee.service;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.exception.ResourceNotFoundException;
import az.ingress.employee.mapper.EmployeeMapper;
import az.ingress.employee.model.Employee;
import az.ingress.employee.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
//This annotation is used to enable the Mockito extension for JUnit 5. It initializes and manages mocks during the test lifecycle
class EmployeeServiceImplTest {
//    "Arrange, Act, Assert" (often abbreviated as AAA) is a pattern commonly used in unit testing to structure and organize your test cases.
//    It helps make tests more readable, maintainable, and easy to understand. Here's what each part of AAA means:
//
//    Arrange:
//
//    In this step, you set up the preconditions for your test. This involves creating objects, initializing variables, and configuring the environment to prepare for the actual test.
//            It's about organizing everything needed for the test scenario.
//    Act:
//
//    This step involves performing the actual operation or action that you want to test.
//    It's the execution of the code or method that you are testing.
//    Assert:
//
//    After performing the action, you check the result against an expected outcome.
//    It involves verifying that the actual result matches what you anticipated.
//    Assertions are used to confirm that the system is behaving as expected.

    @InjectMocks
    //use this annotation on the class under test
    private EmployeeServiceImpl employeeService;

    @Mock
    //use this annotation to create mock objects for the dependencies (fields) of the class you are testing
    private EmployeeRepository employeeRepository;
    @Mock
    private EmployeeMapper employeeMapper;

    private Employee employee;

    private EmployeeDto employeeDto;

    private List<Employee> employeeList;

    private List<EmployeeDto> employeeDtoList;
    private List<Employee> employeeEmptyList;

    private List<EmployeeDto> employeeDtoEmptyList;

    private EmployeeDto updatedEmployeeDto;

    @BeforeEach
    //Use this to annotate a method that should be executed before each test method in the test class.
    public void setUp() {
        ReflectionTestUtils.setField(employeeService, "testField", 1);
        //ReflectionTestUtils used to set or retrieve the values of private fields in objects during testing
        employee = Employee.builder().id(1).firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build();

        employeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build();

        employeeList = Arrays.asList(Employee.builder().id(1).firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build(), Employee.builder().id(2).firstName("Huseyn2").lastName("Guliyev2").email("huseyn2.guliyev2@pashabank.az").phoneNumber("+994502296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(2000)).build());

        employeeDtoList = Arrays.asList(EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build(), EmployeeDto.builder().firstName("Huseyn2").lastName("Guliyev2").email("huseyn2.guliyev2@pashabank.az").phoneNumber("+994502296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(2000)).build());

        employeeEmptyList = Collections.emptyList();

        employeeDtoEmptyList = Collections.emptyList();

        updatedEmployeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Senior Core Banking System Developer").salary(new BigDecimal(2000)).build();

    }

    @Test
        //it signals to the testing framework that this method should be executed as a test case
    void givenValidEmployeeId_whenGetEmployeeById_thenReturnEmployee() {
        // arrange
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(employee));
        when(employeeMapper.mapToEmployeeDto(any())).thenReturn(employeeDto);

        // act
        EmployeeDto returnEmployee = employeeService.getEmployeeById(1);

        // assert
        assertThat(returnEmployee).isNotNull();
        assertThat(returnEmployee.getFirstName()).isEqualTo("Huseyn");
        assertThat(returnEmployee.getLastName()).isEqualTo("Guliyev");
        assertThat(returnEmployee.getEmail()).isEqualTo("huseyn.guliyev@pashabank.az");
        assertThat(returnEmployee.getPhoneNumber()).isEqualTo("+994512296643");
        assertThat(returnEmployee.getJobTitle()).isEqualTo("Core Banking System Developer");
        assertThat(returnEmployee.getSalary()).isEqualTo(new BigDecimal(1000));
        verify(employeeRepository, times(1)).findById(1);
    }

    @Test
    void givenInvalidEmployeeId_whenGetEmployeeById_thenReturnException() {
        // arrange
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> employeeService.getEmployeeById(11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.getEmployeeById(11));
    }


    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmployeesList() {
        // arrange
        when(employeeRepository.findAll()).thenReturn(employeeList);
        when(employeeMapper.mapToEmployeeDtoList(any())).thenReturn(employeeDtoList);

        // act
        List<EmployeeDto> resultEmployeeList = employeeService.getAllEmployees();

        // assert
        assertThat(resultEmployeeList).isNotNull().hasSize(2);
        assertThat(resultEmployeeList.get(0).getFirstName()).isEqualTo("Huseyn");
        assertThat(resultEmployeeList.get(1).getFirstName()).isEqualTo("Huseyn2");
    }

    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmptyList() {
        // arrange
        when(employeeRepository.findAll()).thenReturn(employeeEmptyList);
        when(employeeMapper.mapToEmployeeDtoList(any())).thenReturn(employeeDtoEmptyList);
        when(employeeService.getAllEmployees()).thenReturn(Collections.emptyList());

        // act
        List<EmployeeDto> resultEmployeeList = employeeService.getAllEmployees();

        // assert
        assertThat(resultEmployeeList).isEmpty();
    }

    @Test
    void givenValidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnUpdatedEmployee() {

        when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(employee));
        when(employeeMapper.mapToEmployee(any())).thenReturn(employee);
        when(employeeRepository.save(any())).thenReturn(employee);
        when(employeeMapper.mapToEmployeeDto(any())).thenReturn(updatedEmployeeDto);

        // act
        EmployeeDto savedEmployee = employeeService.updateEmployee(1, employeeDto);

        // assert
        assertThat(savedEmployee.getJobTitle()).isEqualTo("Senior Core Banking System Developer");
        assertThat(savedEmployee.getSalary()).isEqualTo(new BigDecimal(2000));
    }

    @Test
    void givenInvalidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnException() {
        // arrange
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> employeeService.updateEmployee(11, updatedEmployeeDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.updateEmployee(11, updatedEmployeeDto));
    }

    @Test
    void givenValidEmployeeId_whenDeleteEmployee_thenNothing() {
        // arrange
        Employee employee = Employee.builder().id(1).firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build();

        when(employeeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(employee));
        doNothing().when(employeeRepository).deleteById(anyInt());

        // act
        employeeService.deleteEmployee(employee.getId());

        // assert
        verify(employeeRepository, times(1)).deleteById(1);
    }

    @Test
    void givenInvalidEmployeeId_whenDeleteEmployee_thenReturnException() {
        // arrange
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> employeeService.deleteEmployee(11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.deleteEmployee(11));
    }

    @Test
    void givenEmployee_whenSaveEmployee_thenReturnEmployee() {
        // arrange

        when(employeeMapper.mapToEmployee(any())).thenReturn(employee);
        when(employeeRepository.save(any())).thenReturn(employee);
        when(employeeMapper.mapToEmployeeDto(any())).thenReturn(employeeDto);

        // act
        EmployeeDto savedEmployee = employeeService.saveEmployee(employeeDto);

        // assert
        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getFirstName()).isEqualTo("Huseyn");
        assertThat(savedEmployee.getLastName()).isEqualTo("Guliyev");
        assertThat(savedEmployee.getEmail()).isEqualTo("huseyn.guliyev@pashabank.az");
        assertThat(savedEmployee.getPhoneNumber()).isEqualTo("+994512296643");
        assertThat(savedEmployee.getJobTitle()).isEqualTo("Core Banking System Developer");
        assertThat(savedEmployee.getSalary()).isEqualTo(new BigDecimal(1000));
    }

}