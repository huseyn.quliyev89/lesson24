package az.ingress.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorDetails> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.NOT_FOUND.value()).
                code(ErrorCodes.RESOURCE_NOT_FOUND.getCode()).
                message(ex.getMessage()).
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorDetails> badRequestException(HttpMessageNotReadableException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCodes.BAD_REQUEST.getCode()).
                message("Invalid JSON format in the request body").
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
