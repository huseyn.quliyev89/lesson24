package az.ingress.employee.service;

import az.ingress.employee.dto.EmployeeDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface EmployeeService {
    EmployeeDto saveEmployee(EmployeeDto employeeDto);
    EmployeeDto updateEmployee(Integer employeeId, EmployeeDto employeeDto);
    List<EmployeeDto> getAllEmployees();
    EmployeeDto getEmployeeById(Integer employeeId);
    void deleteEmployee(Integer employeeId);



}
