package az.ingress.employee.mapper;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.model.Employee;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    EmployeeDto mapToEmployeeDto(Employee employee);
    Employee mapToEmployee(EmployeeDto employeeDto);
    List<EmployeeDto> mapToEmployeeDtoList(List<Employee> employeeList);
}
